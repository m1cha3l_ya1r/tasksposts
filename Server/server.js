import express from 'express'
import cors from 'cors'
import usersController from './controllers/usersController.js'

let app = express()

app.use(cors())

import './configs/server_config.js'

app.use(express.json())
// first parse incoming data
app.use(express.urlencoded({extended: true}));
// then user the controller
app.use('/users', usersController);
// finally listen to port
app.listen(2000);
console.log('Server is up, listening in port 2000');