// get user schema
import user from '../model/userModel.js'

// GET all Cars data
export function GetAllUsers()
    {
        return new Promise((resolve, reject) =>
        {
            user.find({}, function(err, data)
            {
                if(err)
                {
                    reject(err)
                }
                else
                {
                    resolve(data)
                }
            })
        })
    }


// GET a user data
export function GetAUser(id)
    {
        return new Promise((resolve, reject) =>
        {
            user.findById(id, function(err,data)
            {
                if(err)
                {
                    reject(err)
                }
                else
                {
                    resolve(data)
                }
            })
        })
    }


export function AddUser(data)
    {
        console.log(data);
        return new Promise((resolve, reject) =>
        {
            let toAdd = new user({
                Name : data.Name,
                Email : data.Email,
                Street : data.Street,
                City : data.City,
                Zipcode : data.Zipcode,
                Tasks : data.Tasks,
                Posts : data.Posts
            })
            toAdd.save(function(err)
            {
                if(err)
                {
                    reject(err)
                }
                else
                {
                    resolve('Created!')
                }
            })
        })

    }

export function UpdateUser(id, updateData)
{
    return new Promise((resolve,reject) =>
    {
        findByIdAndUpdate(id, {
            Name: updateData.Name,
            Email : updateData.Email,
            Street : updateData.Street,
            City : updateData.City,
            Zipcode : updateData.Zipcode,
            Tasks : updateData.Tasks,
            Posts : updateData.Posts
        },function(err)
        {
            if(err)
            {
                reject(err)
            }
            else
            {
                resolve('Updated')
            }
        })
    })
}

// Delete
export function Delete(id)
{
    return new Promise((resolve, reject) =>
    {
        findByIdAndDelete(id, function(err)
        {
            if(err)
            {
                reject(err)
            }
            else
            {
                resolve('Deleted!')
            }
        })
    })
}