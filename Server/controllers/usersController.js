// import express for rounting functionality
import { Router } from 'express'
// link to usersUtils
import { GetAllUsers, GetAUser, AddUser, UpdateUser, Delete } from '../model/usersUtils.js'
// link to Router function
const appRoute = Router()


// GET all
appRoute.route('/').get(async function(req,resp)
{
    let users = await GetAllUsers()
    return resp.json(users)
})


// GET a user
appRoute.route('/:id').get(async function(req, resp)
{
    let id = req.params.id;
    let user = await GetAUser(id);
    return resp.json(user)
})


// POST (add a user)
appRoute.route('/').post(async function(req,resp)
{
    let newuser = req.body;
    let result = await AddUser(newuser)
    return resp.json(result)
})

// PUT (update a user)
appRoute.route('/:id').put(async function(req,resp)
{
    let id = req.params.id
    let newuser = req.body
    let result = await UpdateUser(id,newuser)
    return resp.json(result)
})

// Delete a user
appRoute.route('/:id').delete(async function(req,resp)
{
    let id = req.params.id
    let result = await Delete(id)
    return resp.json(result)
})

export default appRoute;