import { Component, Input,  OnInit } from '@angular/core';
// import { emit } from 'process';
import { Subscription } from 'rxjs';
import { UserService } from '../user.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  @Input()
  taskCard: any = {}
  @Input()
  user: any = {}

  subPut: Subscription
  subGet: Subscription
  constructor(private utils: UserService) { }

  setCompleted()
  {
    this.taskCard.completed = true
    this.user.Tasks.map(task => task._id===this.taskCard._id)
    this.subPut = this.utils.putUser(this.user._id, this.user).subscribe(data => {
      console.log(data)
      this.subGet = this.utils.getAUser(this.user._id).subscribe(refresh => {
        this.user=refresh
      })
      
    })
  }

  ngOnInit(): void {
  }

  ngOnDestrouy()
  {
    if(this.subPut!=null)
    {this.subPut.unsubscribe()}
    if(this.subGet!=null)
    {this.subGet.unsubscribe()}
  }

}
