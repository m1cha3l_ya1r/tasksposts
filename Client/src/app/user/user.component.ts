import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  
  @Input()
  singleUser: any = {}
  moreInfoToggle: boolean = false
  subPut: Subscription;
  subDel: Subscription;
  subGet: Subscription;

  constructor(private utils: UserService) { }

  toggleInfo()
  {
    this.moreInfoToggle = true
  }

  closeInfo()
  {
    this.moreInfoToggle = false
  }

  updateUser()
  {
    this.subPut = this.utils.putUser(this.singleUser._id, this.singleUser).subscribe(data => {
      console.log(data)
      this.subGet = this.utils.getAUser(this.singleUser._id).subscribe(refresh => this.singleUser=refresh)
    })
  }

  deleteUser()
  {
    this.subDel = this.utils.deleteUser(this.singleUser._id).subscribe(data => {
      console.log(data)
      window.location.reload()
    })
  }

  userTasksStatus(u: User)
  {
    let overAllTasks: boolean;
    let tasks = []
    u.Tasks.forEach(t => 
      tasks.push(t.completed)
    );
    overAllTasks = tasks.every(x=>x===true)
    
    return overAllTasks
  }


  ngOnInit(): void {
  }

  ngOnDestroy()
  {
    if(this.subPut!=null)
    {
      this.subPut.unsubscribe()
    }

    if(this.subDel!=null)
    {
      this.subDel.unsubscribe()
    }

    if(this.subGet!=null)
    {
      this.subGet.unsubscribe()
    }
  }

}
